# Web Portal

## Default Instance

When you first connect to [http://localhost:4200/](http://localhost:4200/), you should see a default project implemented.
Note the default re-routing to `./portal/home/`.

You should see something like the following.

![web_portal](img/web_portal.png)

You can click on `Access to the project` to go to the homepage of this project.
In a deployed version of ANIS, each specific project here after are called ***instances***.

## A portal example

Naturally, in the case you have configured other projects using ANIS, you should see here their listing.
For an example, you have a look at [this fully configured anis](https://data.lam.fr/)
