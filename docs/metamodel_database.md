# Metamodel database

## Introduction

As an information system ANIS needs at least two databases to function properly:

 - One database to store the configuration, so called *metamodel database*, storing the configuration of the app itself
 - At least one database containing observation datasets, the *scientific database* or *business database*

This page details how the **metamodel database** works.

## Purpose of the metamodel database

ANIS needs a database to store all the configuration for:

 - The available business database(s) that should contains the shared datasets.
 - The different scientific projects.
 - The shared datasets and their attributes.
 - The access rights on datasets (groups_datasets).

## Metamodel schema

![database_schema](img/metamodel_mcd.svg)

## Settings

By default, the `server` docker container is configured to connect to a PostgreSQL database created during the first launch of docker-compose.
The docker compose integrates a [PostgreSQL](https://www.postgresql.org/) database server named `db`.
You can find the `docker-compose.yml` file at the root of the `anis` project.

The database settings connection are given in the environment variables of the `server` container.

![php_settings.png](img/server_settings.png#center)

Here is the list of server container configuration concerning the metamodel database connection:

- `DATABASE_DEV_MODE`: Default to `1` in development mode and to `0` in production mode
- `DATABASE_CO_DRIVER`: Doctrine pdo driver used to connect to the database
- `DATABASE_CO_HOST`: Database server host
- `DATABASE_CO_PORT`: Database server listening port
- `DATABASE_CO_DBNAME`: Name of the server metamodel database 
- `DATABASE_CO_USER`: User name used to connect to the server metamodel database
- `DATABASE_CO_PASSWORD`: Password used to connect to the server metamodel database

You are free to change this configuration to connect to another metamodel database.

## Doctrine entities

ANIS server uses a tool called **doctrine** to generate the database from the code.
You can find more information on the official website: [https://www.doctrine-project.org](https://www.doctrine-project.org).

Anis server stores the different doctrine entities in the `server/src/Entity` folder and each file corresponds to a table in the metamodel database.

![doctrine_entities](img/doctrine_entities.png)

If you want to change the structure of the database you must edit or add entities.
But be careful for each change you will have to re-generate the database and the doctrine proxies.

## Doctrine command-line tool

You can use the doctrine command-line tool to perform most of the operations.
To run the tool, ANIS must be started.

You must be at the root of the ANIS directory.  
You can then enter into the server container by typing the following command:

```bash
$ make shell_server
```

Once in the container you can type the following command to execute doctrine-cli:

```bash
$ ./bin/doctrine
```

What you see are the different operations you can perform.

![doctrine_cli_operations](img/doctrine_cli_operations.png)

## Validate schema

If you have performed operations on entities you must ensure that the database schema remains valid.
Doctrine then proposes a command to perform that:

```bash
$ ./bin/doctrine orm:validate-schema
```

## Create database

If the tables of your metamodel database are not yet generated you must ask doctrine to create them with the following command:

```bash
$ ./bin/doctrine orm:schema-tool:create
```

## Drop database

If the tables of your metamodel database are already generated but you want to start from a blank database you can type the following command:

```bash
$ ./bin/doctrine orm:schema-tool:drop
```

## Update database

If you have made changes to your entities you must update the metamodel database with the following command: 

```bash
$ ./bin/doctrine orm:schema-tool:update
```

## Generate proxies

If you have made changes to your entities you must also update the doctrine entities proxies.
The doctrine entities proxies are used to improve the performance of requests.
To regenerate them type the following command:

```bash
$ ./bin/doctrine orm:generate-proxies
```
