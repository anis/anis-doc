# Server API

## Introduction

ANIS server provides a REST API to perform actions on the metamodel database.

You can use the latter API to configure the app logic remotely or via the web interface as described later on.

This section lists the different endpoints accessible.

You can find the list of all endpoints in the `app/routes.php` file.
Each route refers to a class `Action` and when the endpoint call occurs. On each call the `__invoke` function executes.

## Actions

You can find the `Action` files in the `src/Action` folder.
You can modify one or more actions to change the behavior of an endpoint.
You can also add an action file to add an entrypoint.

If you want to add an new action, you need to declare a new route into the `app/routes.php` file.
You then need to instantiate your new `Action` into the `app/dependencies.php` file.

## Endpoints

### Root

Used to ensure that the service is up and running.

**URL**: `/`

**Method**: `GET`

**URL Params**: none

**Query Params**: none

**Auth required**: no

**Admin privilege required**: no

<p class='success_response' markdown='1'>Success Response:</p>

**Code**: `200 OK`

**Content example**

```json
{
    "message": "it works!"
}
```

### Client settings

Returns information about the ANIS installation. This endpoint is used by web client at startup.

**URL**: `/client-settings`

**Method**: `GET`

**URL Params**: none

**Query Params**: none

**Auth required**: no

**Admin privilege required**: no

<p class='success_response' markdown='1'>Success Response:</p>

**Code**: `200 OK`

**Content example**

```json
{
    "servicesUrl": "http://localhost:5000",
    "baseHref": "/",
    "authenticationEnabled": false,
    "ssoAuthUrl": "http://localhost:8180/auth",
    "ssoRealm": "anis",
    "ssoClientId": "anis-client",
    "adminRoles": "anis_admin,superuser"
}
```

### Retrieve database list

Used to retrieve business database list.

**URL**: `/database`

**Method**: `GET`

**URL Params**:

**Query Params**: none

**Auth required**: yes

**Admin privilege required**: yes

<p class='success_response' markdown='1'>Success Response:</p>

**Code**: `200 OK`

**Content example**

```json
[
    {
        "id": 1,
        "label": "Test",
        "dbname": "anis_test",
        "dbtype": "pdo_pgsql",
        "dbhost": "db",
        "dbport": 5432,
        "dblogin": "anis",
        "dbpassword": "anis"
    }
]
```

### Add a new database

Used to add a new business database.

**URL**: `/database`

**Method**: `POST`

**URL Params**: none

**Query Params**: none

**Body**:

| Name       | Description                                                 | Type   | Available |
|------------|-------------------------------------------------------------|--------|:---------:|
| label      | It is the label of the new entry                            | string |     X     |
| dbname     | Name of the new database                                    | string |     X     |
| dbtype     | Doctrine driver of the database (pdo_pgsql, pdo_mysql, ...) | string |     X     |
| dbhost     | Database server hostname                                    | string |     X     |
| dbport     | Database server listening port                              | int    |     X     |
| dblogin    | User name for connection to the database                    | string |     X     |
| dbpassword | Password for connection to the database                     | string |     X     |

**Auth required**: yes

**Admin privilege required**: yes

<p class='success_response' markdown='1'>Success Response:</p>

**Code**: `201 Created`

**Content example**

```json
[
    {
        "id": 1,
        "label": "Test",
        "dbname": "anis_test",
        "dbtype": "pdo_pgsql",
        "dbhost": "db",
        "dbport": 5432,
        "dblogin": "anis",
        "dbpassword": "anis"
    }
]
```

<p class='error_response' markdown='1'>Error Response:</p>

**Code**: `400 Bad Request`

**Content example**

```json
{
    "message": "400 Bad Request",
    "exception": [
        {
            "type": "Slim\\Exception\\HttpBadRequestException",
            "code": 400,
            "message": "Param dbpassword needed to add a new database",
            "file": "/project/src/Action/DatabaseListAction.php",
            "line": 50
        }
    ]
}
```

### Retrieve a database by ID

Used to retrieve one database by ID.

**URL**: `/database/{id}`

**Method**: `GET`

**URL Params**:

| Name       | Description          | Type   | Available |
|------------|----------------------|--------|:---------:|
| id         | ID of the database   | int    |     X     |

**Query Params**: none

**Auth required**: yes

**Admin privilege required**: yes

<p class='success_response' markdown='1'>Success Response:</p>

**Code**: `200 OK`

**Content example**

```json
{
    "id": 1,
    "label": "Test",
    "dbname": "anis_test",
    "dbtype": "pdo_pgsql",
    "dbhost": "db",
    "dbport": 5432,
    "dblogin": "anis",
    "dbpassword": "anis"
}
```

<p class='error_response' markdown='1'>Error Response:</p>

**Code**: `404 Not Found`

**Content example**

```json
{
    "message": "404 Not Found",
    "exception": [
        {
            "type": "Slim\\Exception\\HttpNotFoundException",
            "code": 404,
            "message": "Database with id 2 is not found",
            "file": "/project/src/Action/DatabaseAction.php",
            "line": 45
        }
    ]
}
```

### Edit a database

Used to edit a business database.

**URL**: `/database/{id}`

**Method**: `PUT`

**URL Params**:

| Name       | Description          | Type   | Available |
|------------|----------------------|--------|:---------:|
| id         | ID of the database   | int    |     X     |

**Query Params**: none

**Body**:

| Name       | Description                                                 | Type   | Available |
|------------|-------------------------------------------------------------|--------|:---------:|
| label      | It is the new label of the entry                            | string |     X     |
| dbname     | New name of the database                                    | string |     X     |
| dbtype     | Doctrine driver of the database (pdo_pgsql, pdo_mysql, ...) | string |     X     |
| dbhost     | Database server hostname                                    | string |     X     |
| dbport     | Database server listening port                              | int    |     X     |
| dblogin    | User name for connection to the database                    | string |     X     |
| dbpassword | Password for connection to the database                     | string |     X     |

**Auth required**: yes

**Admin privilege required**: yes

<p class='success_response' markdown='1'>Success Response:</p>

**Code**: `200 Ok`

**Content example**

```json
{
    "id": 1,
    "label": "New label",
    "dbname": "anis_test",
    "dbtype": "pdo_pgsql",
    "dbhost": "db",
    "dbport": 5432,
    "dblogin": "anis",
    "dbpassword": "anis"
}
```

<p class='error_response' markdown='1'>Error Response:</p>

**Code**: `404 Not Found`

**Content example**

```json
{
    "message": "404 Not Found",
    "exception": [
        {
            "type": "Slim\\Exception\\HttpNotFoundException",
            "code": 404,
            "message": "Database with id 2 is not found",
            "file": "/project/src/Action/DatabaseAction.php",
            "line": 45
        }
    ]
}
```

**Code**: `400 Bad Request`

**Content example**

```json
{
    "message": "400 Bad Request",
    "exception": [
        {
            "type": "Slim\\Exception\\HttpBadRequestException",
            "code": 400,
            "message": "Param dbpassword needed to edit the database",
            "file": "/project/src/Action/DatabaseAction.php",
            "line": 61
        }
    ]
}
```

### Delete a database by ID

Used to delete one database by ID.

**URL**: `/database/{id}`

**Method**: `DELETE`

**URL Params**:

| Name       | Description          | Type   | Available |
|------------|----------------------|--------|:---------:|
| id         | ID of the database   | int    |     X     |

**Query Params**: none

**Auth required**: yes

**Admin privilege required**: yes

<p class='success_response' markdown='1'>Success Response:</p>

**Code**: `200 OK`

**Content example**

```json
{
    "message": "Database with id 2 is removed!",
}
```

<p class='error_response' markdown='1'>Error Response:</p>

**Code**: `404 Not Found`

**Content example**

```json
{
    "message": "404 Not Found",
    "exception": [
        {
            "type": "Slim\\Exception\\HttpNotFoundException",
            "code": 404,
            "message": "Database with id 2 is not found",
            "file": "/project/src/Action/DatabaseAction.php",
            "line": 45
        }
    ]
}
```

### Retrieve table list from a database

**URL**: `/database/{id}/table`

**Method**: `GET`

**URL Params**:

| Name       | Description          | Type   | Available |
|------------|----------------------|--------|:---------:|
| id         | ID of the database   | int    |     X     |

**Query Params**: none

**Auth required**: yes

**Admin privilege required**: yes

<p class='success_response' markdown='1'>Success Response:</p>

**Code**: `200 OK`

**Content example**

```json
[
    "observation",
    "aspic_vipers_dr2_w1",
    "sp_cards",
    "products",
    "aspic_vvds_f02_udeep",
    "public.v_observation"
]
```

<p class='error_response' markdown='1'>Error Response:</p>

**Code**: `404 Not Found`

**Content example**

```json
{
    "message": "404 Not Found",
    "exception": [
        {
            "type": "Slim\\Exception\\HttpNotFoundException",
            "code": 404,
            "message": "Database with id 2 is not found",
            "file": "/project/src/Action/TableListAction.php",
            "line": 58
        }
    ]
}
```

### Admin file explorer

Used to browse the ANIS data directory

**URL**: `/admin-file-explorer[{fpath:.*}]`

**Method**: `GET`

**URL Params**:

| Name       | Description          | Type   | Available |
|------------|----------------------|--------|:---------:|
| fpath      | Path to explore      | string |     X     |

**Query Params**: none

**Auth required**: yes

**Admin privilege required**: yes

<p class='success_response' markdown='1'>Success Response:</p>

**Code**: `200 OK`

**Content example**

```json
[
    {
        "name": ".",
        "size": 4096,
        "type": "dir",
        "mimetype": "directory"
    },
    {
        "name": "..",
        "size": 4096,
        "type": "dir",
        "mimetype": "directory"
    },
    {
        "name": "ARCHIVE",
        "size": 4096,
        "type": "dir",
        "mimetype": "directory"
    },
    {
        "name": "DEFAULT",
        "size": 4096,
        "type": "dir",
        "mimetype": "directory"
    }
]
```

<p class='error_response' markdown='1'>Error Response:</p>

**Code**: `404 Not Found`

**Content example**

```json
{
    "message": "404 Not Found",
    "exception": [
        {
            "type": "Slim\\Exception\\HttpNotFoundException",
            "code": 404,
            "message": "The requested path is not found",
            "file": "/project/src/Action/AdminFileExplorerAction.php",
            "line": 68
        }
    ]
}
```

### Retrieves groups of users with authorized instances

Used to retrieve groups of users with the list of authorized scientific projects

**URL**: `/instance-group`

**Method**: `GET`

**URL Params**: none

**Query Params**: none

**Auth required**: no

**Admin privilege required**: no

<p class='success_response' markdown='1'>Success Response:</p>

**Code**: `200 OK`

**Content example**

```json
[
    {
        "id": 1,
        "role": "GROUP_TEST",
        "instances": [
            "default"
        ]
    }
]
```

### Add a new group of users with authorized instances

Used to add a new business database.

**URL**: `/instance-group`

**Method**: `POST`

**URL Params**: none

**Query Params**: none

**Body**:

| Name       | Description                                                 | Type   | Available |
|------------|-------------------------------------------------------------|--------|:---------:|
| role       | It is the role name of the new entry                        | string |     X     |
| instances  | List of instances authorized for this group                 | array  |     X     |

**Auth required**: yes

**Admin privilege required**: yes

<p class='success_response' markdown='1'>Success Response:</p>

**Code**: `201 Created`

**Content example**

```json
{
    "id": 2,
    "role": "GROUP_TEST",
    "instances": ["default"]
}
```

<p class='error_response' markdown='1'>Error Response:</p>

**Code**: `400 Bad Request`

**Content example**

```json
{
    "message": "400 Bad Request",
    "exception": [
        {
            "type": "Slim\\Exception\\HttpBadRequestException",
            "code": 400,
            "message": "Param role needed to add a new instance-group",
            "file": "/project/src/Action/InstanceGroupListAction.php",
            "line": 59
        }
    ]
}
```

### Retrieves a group of users with authorized instances by ID

Used to retrieve a group of users with the list of authorized scientific projects by ID

**URL**: `/instance-group/{id}`

**Method**: `GET`

**URL Params**:

| Name       | Description          | Type   | Available |
|------------|----------------------|--------|:---------:|
| id         | ID of the group      | int    |     X     |

**Query Params**: none

**Auth required**: no

**Admin privilege required**: no

<p class='success_response' markdown='1'>Success Response:</p>

**Code**: `200 OK`

**Content example**

```json
{
    "id": 2,
    "role": "GROUP_TEST",
    "instances": [
        "default"
    ]
}
```
