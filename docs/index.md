# Welcome to the ANIS documentation

![anis_v3_logo](img/anis_v3_logo300.png#center)

## Introduction

Welcome to the *AstroNomical Information System (ANIS)* user's guide.
Here you can find all the information you need to retrieve, install, and develop with the latest version of ANIS.

ANIS is a web generic tool aimed at facilitating and homogenizing the implementation of astronomical data of various kinds and catalogues in dedicated Information Systems.

ANIS includes:

 - __anis-server__: A JSON *Representational State Transfer (REST)* API that allows an administrator to configure datasets and allows users to search into business databases.
 - __anis-client__: A web client,
    - That allows users to carry out searches and format the results.
    - That contains a dashboard reserved for administrators only, and which allows to set up scientific projects.
 - __anis-services__: An API used by anis-client for fits file manipulation (display fits, fits-cut, etc) with [astropy](https://www.astropy.org/).
 - __anis-tasks__: A `python` command line interface enabling the launch of asynchronous tasks using the [rabbitMQ](https://www.rabbitmq.com/) message broker.

You can find here the complete public repository: [https://gitlab.lam.fr/anis/anis](https://gitlab.lam.fr/anis/anis).

## Functionalities 

 - ANIS provides a complete JSON web API for searching and manipulating astrophysical data.
     - With this API an administrator can configure the visualisation of datasets.
     - Users can search the latter datasets using search criteria.
 - ANIS also provides an administration dashboard to manage datasets and configure search forms.
 - ANIS provides a web interface that allows users to perform, among other things, searches into datasets.
     - Users can perform asynchronous actions from the web interface such as the archive creation for example.
 - Scientific projects and/or datasets accessible through ANIS can be private and reserved to a consortium.
 - ANIS includes specific visualizations tool for data such as spectrum graph, light curve, etc.

## Requirements

To install and develop the local version of the software you need the following tools:

- [git](https://git-scm.com/)
- [docker](https://www.docker.com/)
- Eventually [make](https://www.gnu.org/software/make/manual/make.html) to
  shorten command lines

To follow along the documentation, you also need [curl](https://curl.se/docs/manpage.html).

## About this software

The software ANIS is currently being developed by [a team](index.md#authors) at the *Laboratoire d'Astrophysique de Marseille (LAM)* to meet the needs of the scientific missions in which the LAM is engaged.

![lam_logo](img/lam_logo.jpg#center)

## Licence

AstroNomical Information System is governed by the CeCILL license (Version 2.1 dated 2013-06-21) under French law and abiding by the rules of distribution of free software.

You can use, modify, and/or redistribute the software under the terms of the
CeCILL license as circulated by the *Commissariat à l'énergie atomique et aux
énergies alternatives (CEA)* , the *Centre national de la recherche scientifique (CNRS)*, and the *Institut national de recherche en informatique et en automatique (Inria)* at the following URL
[https://cecill.info](https://cecill.info/index.en.html).

## Authors

Here is the list of people involved in the development:

* _François Agneray_ : Ingénieur de recherche CNRS
* _Chrystel Moreau_ : Ingénieur de recherche CNRS
* _Lucas Menou_ : Ingénieur de recherche - Short Term Contract - CNRS since 2023
* _Divin Angapay_ : Ingénieur d'études - Short Term Contract - CNRS 2022-2023
* _Tifenn Guillas_ : Ingénieur de recherche - Short Term Contract - CNRS 2018-2022
