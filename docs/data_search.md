# Data search

## Introduction

Anis-server provides a single entrypoint to search into a dataset.
For a search in a dataset, the user can specify:

 - Attributes to be displayed
 - Search criteria
 - Sort order
 - Pagination
 - An output format

Entrypoint example to search into `observations` dataset:

```
/search/observations
```

The search parameters must be added after the `?`.
We will see in the following documentation what parameters are availables and how to add them.

```
/search/observations?[PARAMETERS]
```

`Warning`: the parameter `a` is mandatory to perform a search.

## Attributes (param a)

The user can select output attributes by their metamodel ID.
When the dataset is created and the attributes are added, the metamodel assigns an identification number starting from 1.
The user can list the attributes available for a dataset.

```bash
$ curl "http://localhost:8080/dataset/observations/attribute"
```

To add a list of attributes in a dataset search the user must add the query parameter `a`.
It must specify a list of ID separated by a semicolon.

If the user wants attributes 1, 2 and 3:

```
a=1;2;3
```

Example:

```bash
$ curl "http://localhost:8080/search/observations?a=1;2;3"
```

The user can choose the output order of the attributes.
Example if the user wants to see attribute 3 before 2:

```bash
$ curl "http://localhost:8080/search/observations?a=1;3;2"
```

### Count

The user can replace the list of attributes with the special keyword `count`.
This will return the number of records found for a search.
For example, if the user wants to retrieve the number of records in the observations dataset:

```bash
$ curl "http://localhost:8080/search/observations?a=count"
```

if the user wants to retrieve the number of records in the observations dataset when the ID observations is greater than 424:

```bash
$ curl "http://localhost:8080/search/observations?a=count&c=1::gt::424"
```

### All

The user can replace the list of attributes with the special keyword `all`.
This will return all attributes available for the dataset.
For example, if the user wants to retrieve all columns for this dataset observations search:

```bash
$ curl "http://localhost:8080/search/observations?a=all&c=1::gt::424"
```

## Criteria (param c)

The user can apply one or more search criteria to filter the search result.
The search criteria are added on the parameter `c` and each criterion is separated by a `;`.

```
c=c1;c2;c3....
```

Each search criterion is composed of 3 parts:

 - The attribute on which the filter will be performed
 - The operator
 - 0 or n values

Each part must be separated by `::`

Example

```
1::eq::10
```

This filter will search for all values where the attribute 1 is 10.

We will now see the list of available operators.

### Equal

The user can add an equal search criterion using the `eq` operator.
With the `eq` operator the user must specify a single value.
For example if the user wants to search for the observation with the number 418:

```
1::eq::418
```

Complete example with observations dataset:

```bash
$ curl "http://localhost:8080/search/observations?a=1;2;3&c=1::eq::418"
```

### Not equal

The user can add a not equal search criterion using the `neq` operator.
With the `neq` operator the user must specify a single value.
For example if the user wants to search all observations except 418:

```
1::neq::418
```

Complete example with observations dataset:

```bash
$ curl "http://localhost:8080/search/observations?a=1;2;3&c=1::neq::418"
```

### Between

The user can add a between search criterion using the `bw` operator.
With the `bw` operator the user must specify two values separated by `|`.
For example if the user wants to search all observations between 418 and 420:

```
1::bw::418|420
```

Complete example with observations dataset:

```bash
$ curl "http://localhost:8080/search/observations?a=1;2;3&c=1::bw::418|420"
```

### Greater than

The user can add a greater than search criterion using the `gt` operator.
With the `gt` operator the user must specify a single value.
For example if the user wants to search all observations with ID greater than 424:

```
1::gt::424
```

Complete example with observations dataset:

```bash
$ curl "http://localhost:8080/search/observations?a=1;2;3&c=1::gt::424"
```

### Greater than equal

The user can add a greater than equal search criterion using the `gte` operator.
With the `gte` operator the user must specify a single value.
For example if the user wants to search all observations with ID greater than or equal to 424:

```
1::gte::424
```

Complete example with observations dataset:

```bash
$ curl "http://localhost:8080/search/observations?a=1;2;3&c=1::gte::424"
```

### Less than

The user can add a less than search criterion using the `lt` operator.
With the `lt` operator the user must specify a single value.
For example if the user wants to search all observations with ID less than 424:

```
1::lt::424
```

Complete example with observations dataset:

```bash
$ curl "http://localhost:8080/search/observations?a=1;2;3&c=1::lt::424"
```

### Less than equal

The user can add a less than equal search criterion using the `lte` operator.
With the `lte` operator the user must specify a single value.
For example if the user wants to search all observations with ID less than or equal to 424:

```
1::lte::424
```

Complete example with observations dataset:

```bash
$ curl "http://localhost:8080/search/observations?a=1;2;3&c=1::lte::424"
```

### Like

The user can add a like search criterion using the `lk` operator.
With the `lk` operator the user must specify a single value.
For example if the user wants to search observations object name (M 15) which contains the string 15:

```
7::lk::15
```

Complete example with observations dataset:

```bash
$ curl "http://localhost:8080/search/observations?a=1;2;3;7&c=7::lk::15"
```

### Not like

The user can add a like search criterion using the `nlk` operator.
With the `nlk` operator the user must specify a single value.
For example if the user wants to search observations object name which not contains the string 15:

```
7::nlk::15
```

Complete example with observations dataset:

```bash
$ curl "http://localhost:8080/search/observations?a=1;2;3;7&c=7::nlk::15"
```

### In

The user can add a in search criterion using the `in` operator.
With the `in` operator the user must specify multiple values separated by `|`.
For example if the user wants to search observations number 418 and 419 and 420:

```
1::in::418|419|420
```

Complete example with observations dataset:

```bash
$ curl "http://localhost:8080/search/observations?a=1;2;3&c=1::in::418|419|420"
```

### Not in

The user can add a not in search criterion using the `nin` operator.
With the `nin` operator the user must specify multiple values separated by `|`.
For example if the user wants to search observations which do not have the number 418 and 419 and 420:

```
1::nin::418|419|420
```

Complete example with observations dataset:

```bash
$ curl "http://localhost:8080/search/observations?a=1;2;3&c=1::nin::418|419|420"
```

### Null

The user can add a null criterion using `nl` operator.
With the `nl` operator the user must not specify any value.
For example if the user wants to search cards whose type is null:

```
6::nl
```

Complete example with sp_cards dataset:

```bash
$ curl "http://localhost:8080/search/sp_cards?a=1;2;3;4;5;6&c=6::nl"
```

### Not null

The user can add a not null criterion using `nnl` operator.
With the `nnl` operator the user must not specify any value.
For example if the user wants to search cards whose type is not null:

```
6::nnl
```

Complete example with sp_cards dataset:

```bash
$ curl "http://localhost:8080/search/sp_cards?a=1;2;3;4;5;6&c=6::nnl"
```

### Json

The user can add a json criterion using `js` operator.
This operator is a special operator allows you to search in **a json column for PostgreSQL databases only**.
For example if the user wants to search cards when the json_schema contains the key `name` equal to `MXT-EVT-CAL`:

```
7::js::name|eq|MXT-EVT-CAL
```

Complete example with sp_cards dataset:

```bash
$ curl "http://localhost:8080/search/sp_cards?a=1;2;3;4;5;6;7&c=7::js::name|eq|MXT-EVT-CAL"
```

## Cone-search (param cs)

The user has the possibility to perform a cone-search on a dataset to filter the results.
The user must specify a spatial position in degrees and a search radius in arc seconds.

The parameter `cs` requires 3 values separated by `:`:

 - Right ascension in degrees
 - Declination in degrees
 - Search radius in arc seconds

For example if the user wants all observations around of the position 322.5 +12.167 (100 arc seconds):

```
cs=322.5:12.167:100
```

Complete example with observations dataset:

```bash
$ curl "http://localhost:8080/search/observations?a=1;2;3&cs=322.5:12.167:100"
```

Warning: For searching via a cone-search in a dataset the dataset config must contains cone-search parameters 
and flag cone-search enabled must be to true.
Example see `config->cone-search` for dataset observations:

```bash
$ curl "http://localhost:8080/dataset/observations"
```

## Order (param o)

The user has the possibility to sort the results by attributes and in ascending or descending order.
The user can perform several sortings. For example, sort by date and then for each date by alphabetical order.

Each order `o` must contain:

 - The attribute ID on which the sorting is performed
 - The meaning `a` for ascending and `d` for descending

If there are several orders, each sort must be separated by `;`

For example if the user wants to sort all observations by date and for each date by object name descending:

```
o=4:a;7:d
```

Complete example with observations dataset:

```bash
$ curl "http://localhost:8080/search/observations?a=1;2;3;4;5;6;7&o=4:a;7:d"
```

## Pagination (param p)

The user has the possibility to return the result using a pagination.
This is very useful when the result is too large.
The user can choose the number of results per page and the page to be displayed.

The parameter `p` requires 2 values:

 - The number of results per page
 - The page to be displayed

For example if the user wants to see the first page with 2 results per page:

```
p=2:1
```

Pagination requires ordering the result for example by observations ID.
Complete example with observations dataset:

```bash
$ curl "http://localhost:8080/search/observations?a=1;2;3&o=1:a&p=2:1"
```

## Result format

The user can choose the format of the search result with the parameter `f`.
By default the result is returned in a json format.

The user can choose between 4 formats: 

- `json`
- `csv`
- `ascii` (values separated by one space)
- `votable`

For example if the user wants all observations in csv format:

```bash
$ curl "http://localhost:8080/search/observations?a=1;2;3&f=csv"
```

## Trajectory parameter (param s)

For the client, an optional parameter `s` exists. The latter indicates on which step a user passed on. The steps `criteria`, `output` or `result` precisely.
While searching each step takes 1 if passed, 0 if not.

For instance, if the user passed through all them while searching a dataset, you should see in the search bar something like:
```
"http://localhost:4200/instance/default/search/result/observed_spectra?s=111&a=1;3;4;5;7;8;9;11"
```

If you stopped at the result table, you should see:
```
"http://localhost:4200/instance/default/search/result/observed_spectra?s=110&a=1;3;4;5;7;8;9;11"
```

This parameter is _not of use for a search, and can be omitted safely_ while performing a query.
This mention is made for the sake of completeness and clarity.
