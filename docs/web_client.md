# Web client

## Introduction

While ANIS offers a [REST API](data_search.md) to query scientific datasets, a web interface to search through available data is often much more convenient.
A web interface can be configured for each projects, such an interface is named **instance** hereafter.

ANIS offers the possibility to fully configure each instance or projects separately.
As such, this page doesn't aim at covering exhaustively all the possibility for each instances.

Each instances should have a dedicated documentation.

The core of ANIS offers a refined way to **search by criteria** among the datasets configured for a particular instance.

This page details such a research based on the default instance provided upon first build.

## Search management

### Selecting a dataset

On first build, on the home page of the default instance, you should see an orange button *Go to search form*.
The latter button directs to the core **search by criteria** that ANIS offers.

![default_search](img/default_search.png)

You should land on the following page.

![dataset_search](img/dataset_search.png)

You can then select the dataset of your choice to search into.
For an example, try the **VIPERS-W1 (DR2)** dataset.

**Note**: datasets are grouped into families. Families of datasets can be defined as related datasets of common interest.
For instance, on the previous picture the *Default dataset family*.

### Adding criteria

A form should now appear in front of you.

![criteria](img/criteria.png)

As a user, you can't configure the previous form.
For that contact the administrator or have a look at the [web interface for administrators documentation](web_admin.md)

Search criteria are proposed to query to **VIPERS-W1** dataset.
As an example, try to fill in the value `101121877` on the `num` criterion.

**Note**: you don't have to fill in all the criteria to refine a research. You
can even leave them empty meaning, you don't refine your search to
get all the dataset.

Try to click in the **Output columns** buttons to configure the result and
the columns of the dataset you want to display on the result table.

### Selecting the outputs columns

On the last step of the search, you can then select what you wish to view on the result table.

![output_columns](img/output_columns.png)

You can either select all the columns to display, or only a few ones.
For that try to tick `delta` and `selmag` boxes. You then selected three columns to display on the result table, with `num` by default selected.

Click on the **result table** to get you result.

### Find your result

In the result table, you should normally see one line, with the `num` criteria you provided respected.
The table should contains three columns, the three columns you selected earlier.

![result_table](img/result_table.png)

**Note**: you have the possibility to download the result table under different formats namely `json`, `ascii`, or `csv`.

You have done a typical search with the ANIS system.
Please note that depending on the configuration of the instance or project you investigate, the search system may be slightly different.
For further information, you need to contact the administrator of the instance you use.

### The detail page

Depending on the dataset you consider, and again its configuration, a so-called **detail page** can be provided for each line of your dataset.

To get an example of such a detail page, please hover on the single `num` criteria of your previous search.
The modification of the pointer should hint a clickable link.

![hover_result](img/hover_result.png)

Upon click, you should land on the detail page, presenting more pieces of information and a graph.

![detail_page](img/detail_page.png)

By now, you got a quick overview of the possibility of the web interface of
ANIS, ranging from dataset search to displaying fits graph and images.
